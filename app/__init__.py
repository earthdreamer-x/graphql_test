from flask import Flask, send_from_directory, request, session, flash, render_template, redirect, Response
import hashlib
from flask_graphql import GraphQLView
from database.schemas.task_schema import schema
from database import baseconnection

hashsalt = 'LNn39kdnf9@#)($k2342'

def verifypass(password):
    thepass = hashsalt + password
    shapass = hashlib.sha256(thepass.encode('utf-8')).hexdigest()
    return shapass

def create_app(config_name):

    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'Kslndfi9IKl13kjp0{@(#*$Y@P#$IOJKdm,ndjuiojrmw'

    @app.route('/')
    def home():
        baseconnection.init_db()
        return Response(response={'OK': 'OK'}, status=200)

    app.add_url_rule(
        '/graphql',
        view_func=GraphQLView.as_view(
            'graphql',
            schema=schema,
            graphiql=True
        )
    )
    return app
