# GraphQL TEST

### Install prerequisites

##### MacOS 
####### If no Homebrew installed:
```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

####### After installation of Homebrew:
```
$ brew install python3
$ curl https://bootstrap.pypa.io/get-pip.py | python3
$ git clone https://gitlab.com/earthdreamer-x/graphql_test.git
$ cd graphql_test
$ pip3 install requirements.txt
$ python run_graphql.py
```

### Usage

* 브라우저로 http://127.0.0.1:5000 으로 접속
* OK 뜨면 데이터베이스 접속 완료
* http://127.0.0.1:5000/graphql 접속
* 다음과 같은 query 날려본다:

```
{
  allUsers {
    edges {
      node {
        id
        name
        isApproved
      }
    }
  }
}
```

* 답변은: 

```
{
  "data": {
    "allUsers": {
      "edges": [
        {
          "node": {
            "id": "VXNlcjo1YzUwMGE5NzBiYmUwYTQ4MWExNWMwZTc=",
            "name": "Client Users",
            "isApproved": null
          }
        },
        {
          "node": {
            "id": "VXNlcjo1YzUwMGE5NzBiYmUwYTQ4MWExNWMwZTk=",
            "name": "Tracy",
            "isApproved": true
          }
        }
      ]
    }
  }
}
```
* Task에 대한 query는:

```
{
  allTasks {
  edges {
    node {
      id
      name
      content
      regDate
    }
  }
}
}
```

* 답변은:

```
{
  "data": {
    "allTasks": {
      "edges": [
        {
          "node": {
            "id": "VGFzazo1YzUwMGE5NzBiYmUwYTQ4MWExNWMwZTg=",
            "name": "Tasks",
            "content": null,
            "regDate": "2019-01-29T17:11:02.453000"
          }
        },
        {
          "node": {
            "id": "VGFzazo1YzUwMGE5NzBiYmUwYTQ4MWExNWMwZWE=",
            "name": "Task1",
            "content": "FIRST ! TASK !",
            "regDate": "2019-01-29T17:11:02.453000"
          }
        }
      ]
    }
  }
}
```

* 실제 만들어지는 Task와 User는 ./database/baseconnection -> init_db() 함수에 있어 변경해도 된다:

```
    tracy = Users(name='Tracy', is_approved=True)
    tracy.save()

    task1 = Tasks(name='Task1', content="FIRST ! TASK !", belongs_to=tracy)
    task1.save()
    
    xxx = Users(name="Igor", is_approved=True)
    xxx.save()
    
    newtask = Tasks(name="TODO", content="MUST DO EVERYTHING", belongs_to=xxx)
    newtask.save()
```

* Query는 묶을 수도 있다:

```
{
  allTasks {
    edges {
      node {
        id
        name
        content
        regDate
        belongsTo {
          id
          name
          isApproved
        }
      }
    }
  }
}
```

* 답변은:

```
{
  "data": {
    "allTasks": {
      "edges": [
        {
          "node": {
            "id": "VGFzazo1YzUwMGI3YjBiYmUwYTQ4MzZjZTU3ZTg=",
            "name": "Tasks",
            "content": null,
            "regDate": "2019-01-29T17:14:47.826000",
            "belongsTo": null
          }
        },
        {
          "node": {
            "id": "VGFzazo1YzUwMGI3YjBiYmUwYTQ4MzZjZTU3ZWE=",
            "name": "Task1",
            "content": "FIRST ! TASK !",
            "regDate": "2019-01-29T17:14:47.826000",
            "belongsTo": {
              "id": "VXNlcjo1YzUwMGI3YjBiYmUwYTQ4MzZjZTU3ZTk=",
              "name": "Tracy",
              "isApproved": true
            }
          }
        }
      ]
    }
  }
}
```

* 모데일 정의는 ./database/models/test_models.py에서 있다:

```python
class Tasks(Document):

    meta = {'collection': 'tasks'}
    name = StringField()
    content = StringField()
    reg_date = DateTimeField(default=datetime.now())
    belongs_to = ReferenceField(Users)
```