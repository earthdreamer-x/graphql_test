import graphene
from graphene.relay import Node
from graphene_mongo import MongoengineConnectionField, MongoengineObjectType
from database.models.test_models import Users as UserModel
from database.models.test_models import Tasks as TaskModel



class Task(MongoengineObjectType):

    class Meta:
        model = TaskModel
        interfaces = (Node,)

class User(MongoengineObjectType):

    class Meta:
        model = UserModel
        interfaces = (Node,)


class Query(graphene.ObjectType):
    node = Node.Field()
    all_users = MongoengineConnectionField(User)
    all_tasks = MongoengineConnectionField(Task)


schema = graphene.Schema(query=Query, types=[Task, User])