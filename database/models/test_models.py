from datetime import datetime
from mongoengine import Document, EmbeddedDocument
from mongoengine.fields import (DateTimeField, EmbeddedDocumentField,
                                ListField, ReferenceField, StringField,
                                BooleanField)



class Users(Document):

    meta = {'collection': 'users'}
    name = StringField()
    is_approved = BooleanField()


class Tasks(Document):

    meta = {'collection': 'tasks'}
    name = StringField()
    content = StringField()
    reg_date = DateTimeField(default=datetime.now())
    belongs_to = ReferenceField(Users)