from mongoengine import connect
from database.models.test_models import Tasks, Users
from datetime import datetime

connect('graphql', host='mongomock://localhost', alias='default')


def init_db():

    print('INITIATING DATABASE')

    users = Users(name='Client Users')
    users.save()

    tasks = Tasks(name='Tasks')
    tasks.save()

    tracy = Users(name='Tracy', is_approved=True)
    tracy.save()

    task1 = Tasks(name='Task1', content="FIRST ! TASK !", belongs_to=tracy)
    task1.save()

